<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('category_id');
            $table->foreignId('supplier_id');
            $table->string('name');
            $table->string('brand')->nullable();
            $table->string('avatar')->nullable();
            $table->string('code')->nullable();
            $table->decimal('purchase_price', 8, 2)->nullable();
            $table->decimal('sale_price', 8, 2)->nullable();
            $table->string('manufacture')->nullable();
            $table->double('tax', 8, 2)->nullable();
            $table->boolean('is_active')->default(1)->nullable();
            $table->boolean('is_blocked')->default(0)->nullable();
            $table->softDeletes(); // <-- This will add a deleted_at field
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
