====== Authentication ========
Register : https://inv.webpointbd.com/api/register
Login : https://inv.webpointbd.com/api/login
Logout : https://inv.webpointbd.com/api/logout
ShowUser : https://inv.webpointbd.com/api/user/profile
Update Profile : https://inv.webpointbd.com/api/user/update
Update Password : https://inv.webpointbd.com/api/user/update/password
show : https://inv.webpointbd.com/api/user/profile

==========Category=========
Index : http://inv.webpointbd.com/api/category
Store : http://inv.webpointbd.com/api/category
Show : http://inv.webpointbd.com/api/category/3
Update : http://inv.webpointbd.com/api/category/3
Delete : http://inv.webpointbd.com/api/category/3

==========Supplier=========
Index : http://inv.webpointbd.com/api/supplier
Store : http://inv.webpointbd.com/api/supplier
Show : http://inv.webpointbd.com/api/supplier/3
Update : http://inv.webpointbd.com/api/supplier/3
Delete : http://inv.webpointbd.com/api/supplier/3
Search : http://inv.webpointbd.com/api/search/supplier/SupplierName

==========customer=========
Index : http://inv.webpointbd.com/api/customer
Store : http://inv.webpointbd.com/api/customer
Show : http://inv.webpointbd.com/api/customer/3
Update : http://inv.webpointbd.com/api/customer/3
Delete : http://inv.webpointbd.com/api/customer/3
Search : http://inv.webpointbd.com/api/search/customer/customerName

==========Product=========
Index : http://inv.webpointbd.com/api/product
Store : http://inv.webpointbd.com/api/product
Show : http://inv.webpointbd.com/api/product/3
Update : http://inv.webpointbd.com/api/product/3
Delete : http://inv.webpointbd.com/api/product/3
Search : http://inv.webpointbd.com/api/search/product/productName

==========Stock=========
Index : http://inv.webpointbd.com/api/stock
Store : http://inv.webpointbd.com/api/stock
Show : http://inv.webpointbd.com/api/stock/3
Update : http://inv.webpointbd.com/api/stock/3