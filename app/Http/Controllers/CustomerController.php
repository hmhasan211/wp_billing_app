<?php

namespace App\Http\Controllers;
use App\Http\Resources\web\CustomerCollection;
use Illuminate\Http\Request;
use App\Models\Customer;
class CustomerController extends Controller
{
      //changeS Tatus
      public function status($id)
      {
  
          $status = Customer::find($id);
          if ($status->is_active == 1) {
              $status->is_active = 0;
          } else {
              $status->is_active = 1;
          }
          $status->save();
          return response()->json([
              'msg' => 'Status has been changed!!',
          ], 200);
      }
      //blockStatus change
      public function blockStatus($id)
      {
          $status = Customer::find($id);
          // dd($status);
          if ($status->is_blocked == 1) {
              $status->is_blocked = 0;
          } else {
              $status->is_blocked = 1;
          }
          $status->save();
          return response()->json([
              'msg' => 'Status has been changed!!',
          ], 200);
      }
  
      //ajax request
      public function getCustomerJson()
      {
          return new CustomerCollection(Customer::query()->with('user')->orderBy('id', 'DESC')->paginate(5));
          // return response()->json([
          //     'success' => 'OK',
          //     'data' => $categories
          // ], 200);
      }
  
      //search data
      public function search($query)
      {
          return new CustomerCollection(Customer::query()->where('name', 'LIKE', "%$query%")->orWhere('email', 'LIKE', "%$query%")->orWhere('phone', 'LIKE', "%$query%")->latest()->paginate(5));
      }
}
