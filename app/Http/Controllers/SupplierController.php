<?php

namespace App\Http\Controllers;

use App\Models\api\Supplier;
use Illuminate\Http\Request;
use App\Http\Resources\web\SupplierCollection;

class SupplierController extends Controller
{
     //changeS Tatus
    //  public function status($id)
    //  {
 
    //      $status = Supplier::find($id);
    //      // dd($status);
    //      if ($status->is_active == 1) {
    //          $status->is_active = 0;
    //      } else {
    //          $status->is_active = 1;
    //      }
    //      $status->save();
    //      return response()->json([
    //          'msg' => 'Status has been changed!!',
    //      ], 200);
    //  }
     //blockStatus change
     public function blockStatus($id)
     {
         $status = Supplier::find($id);
         // dd($status);
         if ($status->is_blocked == 1) {
             $status->is_blocked = 0;
         } else {
             $status->is_blocked = 1;
         }
         $status->save();
         return response()->json([
             'msg' => 'Status has been changed!!',
         ], 200);
     }
 
     //ajax request
     public function getSupplierJson()
     {
         return new SupplierCollection(Supplier::query()->with('user')->orderBy('id', 'DESC')->paginate(5));
     }
 
     //search data
     public function search($query)
     {
         return new SupplierCollection(Supplier::query()->where('name', 'LIKE', "%$query%")->orWhere('email', 'LIKE', "%$query%")->orWhere('phone', 'LIKE', "%$query%")->latest()->paginate(5));
     }
}
