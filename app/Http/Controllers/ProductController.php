<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Resources\web\ProductCollection;

class ProductController extends Controller
{
      //blockStatus change
      public function blockStatus($id)
      {
          $status = Product::find($id);
          // dd($status);
          if ($status->is_blocked == 1) {
              $status->is_blocked = 0;
          } else {
              $status->is_blocked = 1;
          }
          $status->save();
          return response()->json([
              'msg' => 'Status has been changed!!',
          ], 200);
      }
  
      //ajax request
      public function getProductJson()
      {
          return new ProductCollection(Product::query()->with('user')->orderBy('id', 'DESC')->paginate(5));
      }
  
      //search data
      public function search($query)
      {
         $allData = Product::query()
            ->where('name', 'LIKE', "%$query%")
            ->orWhere('brand', 'LIKE', "%$query%")
            // ->whereHas('user',function ($q)use($query)
            // {
            //    return $q->orWhere('name','LIKE', "%$query%");
            // })
            ->latest()
            ->paginate(5);
          
         return new ProductCollection($allData);
      }
}
