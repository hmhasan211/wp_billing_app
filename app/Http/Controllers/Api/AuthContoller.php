<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class AuthContoller extends Controller
{

    public function showProfile()
    {
        $user = auth()->user();
        return new UserResource($user);
    }

    public function login(Request $request)
    {
        $request->validate([
            'mobile' => 'required|numeric',
            'password' => 'required',
        ]);

        $reqData = request()->only('mobile', 'password');
        if (Auth::attempt($reqData)) {
            $user = Auth::user();
            $user['token'] = $user->createToken('userToken')->plainTextToken;

            return new UserResource($user);
        } else {
            return response([
                'message' => 'Sorry! These credentials do not match our records.'
            ], 401);
        }
    }

    //user register
    public function register(Request $request)
    {
        $input = $request->validate([
            'name' => 'required|string',
            'email' => 'nullable|email|unique:users,email',
            'password' => 'required|confirmed|min:8',
            'mobile' => 'required',
            // 'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'dob' => 'date',
            'gender_id' => 'numeric'
        ]);

        $image = $request->file('avatar');
        $slug  = Str::slug($input['name']);
        if (isset($image)) {
            //make unique name
            $imageName   = $slug . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
            
            //check directory exist
            if (!Storage::disk('public')->exists('profile_pic')) {
                Storage::disk('public')->makeDirectory('profile_pic');
            }
            Storage::disk('public')->put('profile_pic/' . $imageName, File::get($image));

        } else {
            $imageName = 'default.png';
        }

        $user = User::create([
            'name' => $input['name'],
            'email' => $request->email,
            'mobile' => $input['mobile'],
            'dob' => $request->dob,
            'password' => bcrypt($input['password']),
            'gender_id' => $request->gender_id,
            'avatar' => $imageName
        ]);

        $user['token'] = $user->createToken('userToken')->plainTextToken;
      
        return response()->json(['data'=>new UserResource($user)],200);
    }

    //update profile info
    public function update(Request $request)
    { 
        $user=auth()->user();
        $input = $request->validate([
            'name' => 'required|string',
            'email' => 'nullable|email|unique:users,email,' . $user->id,
            // 'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'dob' => 'date',
            'gender_id' => 'numeric'
        ]);
       
        $image = $request->file('avatar');
        $slug  = Str::slug($input['name']);
        if (isset($image)) {
            //make unique name
            $imageName   = $slug . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            //check directory exist
            if (!Storage::disk('public')->exists('profile_pic')) {
                Storage::disk('public')->makeDirectory('profile_pic');
            }

            //delete old image
            if (Storage::disk('public')->exists('profile_pic/' . $user->avatar)) {
                Storage::disk('public')->delete('profile_pic/' . $user->avatar);
            }
            Storage::disk('public')->put('profile_pic/' . $imageName, File::get($image));
        } else {
            $imageName =  $user->avatar;
        }

         $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'dob' => $request->dob,
            'gender_id' => $request->gender_id,
            'avatar' => $imageName
        ]);

        return response()->json(['data'=>new UserResource($user)],200);
    }

    //Change Password
    public function updatePassword(Request $request)
    {
        $user=auth()->user();
        $request->validate([
            'old_password' => 'required| different:password',
            'password' => ['required', 'min:8', 'confirmed']
        ]);

        if (Hash::check($request->old_password, $user->password)) {


            // return 'passwrod changed';
            $user->update([
                'password' => Hash::make($request->password)
            ]);
            auth('sanctum')->user()->tokens()->delete();
            return response()->json([
                'message' => 'Password Updated Successfully!'
            ], 200);
        } else {
            return response()->json([
                'message' => ' Old password does not matched!'
            ], 422);
        }
    }

    //User Logout
    public function logout()
    {
        auth('sanctum')->user()->tokens()->delete();

        return response()->json(['message' => 'User logout successfully!']);
    }
}
