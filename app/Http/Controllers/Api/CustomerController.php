<?php

namespace App\Http\Controllers\Api;

use App\Models\api\Customer;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\CustomerCollection;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = Customer::query()->User()->OrderBy('id', 'DESC')->get();
        if ($allData->count() > 0) {
            return new CustomerCollection($allData);
        } else {
            return response()->json(['message' => 'You dont have any Customer']);
        }
    }


    public function search($query)
    {
        $allData = Customer::query()->User()
            ->where('name', 'LIKE', "%$query%")
            ->orWhere('phone', 'LIKE', "%$query%")->OrderBy('id', 'DESC')
            ->orWhere('email', 'LIKE', "%$query%")->OrderBy('id', 'DESC')
            ->get();
        if ($allData->count() > 0) {
            return new CustomerCollection($allData);
        } else {
            return response()->json(['message' => "Sorry!! Your query doesn't match any data "]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|numeric',
            'email' => 'email|sometimes',
            'address' => 'max:200',
            'dob' => 'date',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $image = $request->file('avatar');
        $slug  = Str::slug($request->name);
        if (isset($image)) {
            //make unique name
            $imageName   = $slug . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            //check directory exist
            if (!Storage::disk('public')->exists('customer')) {
                Storage::disk('public')->makeDirectory('customer');
            }
            Storage::disk('public')->put('customer/' . $imageName, File::get($image));
        } else {
            $imageName = 'default.png';
        }

        $addData = Customer::query()->create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'dob' => $request->dob,
            'balance' => $request->balance,
            'avatar' => $imageName,
        ]);

        return response()->json(['message' => 'Customer created successfully!']);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editData = Customer::find($id);

        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|numeric',
            'email' => 'email',
            'address' => 'max:200',
            'dob' => 'date',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $image = $request->file('avatar');
        $slug  = Str::slug($request->name);
        if (isset($image)) {
            //make unique name
            $imageName   = $slug . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            //check directory exist
            if (!Storage::disk('public')->exists('customer')) {
                Storage::disk('public')->makeDirectory('customer');
            }
            //delete old image
            if (Storage::disk('public')->exists('customer/' . $editData->avatar)) {
                Storage::disk('public')->delete('customer/' . $editData->avatar);
            }
            Storage::disk('public')->put('customer/' . $imageName, File::get($image));
        } else {
            $imageName = 'default.png';
        }

        $addData =  $editData->query()->findOrFail($id)->update([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'dob' => $request->dob,
            'balance' => $request->balance,
            'avatar' => $imageName
        ]);

        return response()->json(['message' => 'Customer updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteData = Customer::query()->findOrFail($id)->delete();
        return response()->json(['message' => 'Customer Deleted successfully!']);
    }
}
