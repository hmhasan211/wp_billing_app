<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SupplierCollection;
use App\Models\api\Supplier;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $allData = Supplier::query()->User()->OrderBy('id', 'DESC')->get();
        if ($allData->count() > 0) {
            return new SupplierCollection($allData);
        } else {
            return response()->json(['message' => 'You dont have any Supllier']);
        }
    }

    public function search($query)
    {
        $allData = Supplier::query()->User()
            ->where('name', 'LIKE', "%$query%")
            ->orWhere('phone', 'LIKE', "%$query%")->OrderBy('id', 'DESC')
            ->orWhere('email', 'LIKE', "%$query%")->OrderBy('id', 'DESC')
            ->get();

        if ($allData->count() > 0) {
            return new SupplierCollection($allData);
        } else {
            return response()->json(['message' => "Sorry!! Your query doesn't match any data "]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|numeric',
            'email' => 'email',
            'address' => 'max:200',
            'dob' => 'date',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'balance' => 'numeric'
        ]);

        $image = $request->file('avatar');
        $slug  = Str::slug($request->name);
        if (isset($image)) {
            //make unique name
            $imageName   = $slug . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            //check directory exist
            if (!Storage::disk('public')->exists('supplier')) {
                Storage::disk('public')->makeDirectory('supplier');
            }
            Storage::disk('public')->put('supplier/' . $imageName, File::get($image));
        } else {
            $imageName = 'default.png';
        }

        $addData = Supplier::query()->create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'dob' => $request->dob,
            'balance' => $request->balance,
            'avatar' => $imageName,
        ]);

        return response()->json(['message' => 'Supplier created successfully!']);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $editData = Supplier::find($id);

        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|numeric',
            // 'email' => 'email',
            'address' => 'max:200',
            'dob' => 'date',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $image = $request->file('avatar');
        $slug  = Str::slug($request->name);
        if (isset($image)) {
            //make unique name
            $imageName   = $slug . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            //check directory exist
            if (!Storage::disk('public')->exists('supplier')) {
                Storage::disk('public')->makeDirectory('supplier');
            }
            //delete old image
            if (Storage::disk('public')->exists('supplier/' . $editData->avatar)) {
                Storage::disk('public')->delete('supplier/' . $editData->avatar);
            }
            Storage::disk('public')->put('supplier/' . $imageName, File::get($image));
        } else {
            $imageName = 'default.png';
        }

        $addData =  $editData->query()->findOrFail($id)->update([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'dob' => $request->dob,
            'balance' => $request->balance,
            'avatar' => $imageName
        ]);

        return response()->json(['message' => 'Supplier updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteData = Supplier::query()->findOrFail($id)->delete();
        return response()->json(['message' => 'Supplier Deleted successfully!']);
    }
}
