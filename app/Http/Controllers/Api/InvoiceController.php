<?php

namespace App\Http\Controllers\Api;

use App\Models\api\Stock;
use App\Models\api\Invoice;
use App\Models\api\Product;
use Illuminate\Http\Request;
use App\Models\api\InvoiceProduct;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    public function index()
    {
        return  Invoice::get();
    }

    public function store(Request $request)
    {
        $invoiceData = [
            'user_id'=> auth()->user()->id,
            'total_item' =>$request->total_item,
            'customer_id' =>$request->customer_id,
            'sub_total' =>$request->sub_total,
            'discount' =>$request->discount,
            'vat' =>$request->vat,
            'tax' =>$request->tax,
            'total' =>$request->total,
            'date'=>date('Y-m-d H:i:s'),
        ];
         $insertData =  Invoice::create($invoiceData);
      
         $products =  $request->product;
        //  return $products;
         foreach($products as $pro)
         {
             $data = [
                 'invoice_id'=>$insertData->id,
                 'product_id'=>$pro['product_id'],
                 'qty'=>$pro['qty'],
                 'unit'=>$pro['unit'],
                 'unit_price'=>$pro['unit_price'],
                 'discount'=>$pro['discount'] ?? null,
                 'vat'=>$pro['vat'] ?? null,
                 'tax'=>$pro['tax'] ?? null,
                 'total'=>$pro['total'] ?? null,
             ];
           $invPro =  InvoiceProduct::create($data);
            stock::where('product_id',$pro['product_id'])->decrement('qty', $pro['qty']);
         }
        return response()->json(['message'=>"Data Submited Successfully!!", 'Invoice'=> $insertData, 'invoice_product'=> $invPro
    ]);
      
    }
}

