<?php

namespace App\Http\Controllers\Api;

use App\Models\api\Stock;
use App\Models\api\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Http\Resources\ProductResouce;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\ProductCollection;

class ProductController extends Controller
{
    // stock increment from product
    public function stockIncrement(Request $request,$id)
    {
         $pro =  Product::query()->User()->with('stock')->findOrFail($id);

            if($request->purchase_price || $request->sale_price) {
                $pro->update([
                'purchase_price'=>$request->purchase_price ?? $pro->purchase_price,
                'sale_price'=>$request->sale_price ?? $pro->sale_price,
                 ]);
            }
         if($request->qty){
              $pro->stock->increment('qty',$request->qty);
         }
       
        return response()->json(['message' => 'Data updated successfully!!']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = Product::query()->User()->OrderBy('id', 'DESC')->get();

        if (!is_null($allData)) {
            return new ProductCollection($allData);
        } else {
            return response()->json(['message' => 'You dont have any Product']);
        }
    }

    /**
     * search  in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function search($query)
    {
        $allData = Product::query()->User()
            ->where('name', 'LIKE', "%$query%")
            ->orWhere('brand', 'LIKE', "%$query%")->OrderBy('id', 'DESC')
            ->orWhere('code', 'LIKE', "%$query%")->OrderBy('id', 'DESC')
            ->get();
        if ($allData->count() > 0) {
            return new ProductCollection($allData);
        } else {
            return response()->json(['message' => "Sorry!! Your query doesn't match any data "]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'supplier_id' => 'required|integer',
            'category_id' => 'required|integer',
            'name' => 'required|string',
            'brand' => 'string',
            'code' => 'string',
            'manufacture' => 'string',
            'purchase_price' => 'required|numeric',
            'sale_price' => 'required|numeric',
            'tax' => 'numeric',
            // 'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'qty' =>'nullable',
            'unit' =>'nullable'
        ]);
        $image = $request->file('avatar');
        $slug  = Str::slug($request->name);
        if (isset($image)) {

            //make unique name
            $imageName   = $slug . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            //check directory exist
            if (!Storage::disk('public')->exists('product')) {
                Storage::disk('public')->makeDirectory('product');
            }
            Storage::disk('public')->put('product/' . $imageName, File::get($image));
        } else {
            $imageName = 'product_default.png';
        }

        $data = [
            'user_id' => auth()->user()->id,
            'category_id' => $request->category_id,
            'supplier_id' => $request->supplier_id,
            'name' => $request->name,
            'brand' => $request->brand,
            'code' => $request->code,
            'avatar' => $imageName,
            'purchase_price' => $request->purchase_price,
            'sale_price' => $request->sale_price,
            'manufacture' => $request->manufacture,
            'tax' => $request->tax,
        ];
        $addData = Product::query()->create($data);
        if ($request->qty || $request->unit ||$request->conversion_rate || $request->base_unit) {
            Stock::create([
                'product_id' => $addData->id, 
                'base_unit'=>$request->base_unit,
                'conversion_rate'=>$request->conversion_rate,
                'qty' => $request->qty, 
                'unit' => $request->unit]);
        }

        if (is_null($addData)) {
            return response()->json(['message' => 'Failed to add data!!']);
        } else {
            return new ProductResouce($addData);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showData = Product::query()->User()->findOrFail($id);

        if (is_null($showData)) {
            return response()->json(['message' => 'Data not found!!']);
        } else {
            return new ProductResouce($showData);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editData = Product::query()->User()->find($id);  //dd($editData);
        $this->validate($request, [
            'supplier_id' => 'required|integer',
            'category_id' => 'required|integer',
            'name' => 'required|string',
            'brand' => 'string',
            'code' => 'string',
            'manufacture' => 'string',
            'purchase_price' => 'integer',
            'sale_price' => 'integer',
            'tax' => 'numeric',
            // 'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $image = $request->file('avatar');
        $slug  = Str::slug($request->name);
        if (isset($image)) {
            //make unique name
            $imageName   = $slug . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            //check directory exist
            if (!Storage::disk('public')->exists('product')) {
                Storage::disk('public')->makeDirectory('product');
            }
            //delete old image
            if (Storage::disk('public')->exists('product/' . $editData->avatar)) {
                Storage::disk('public')->delete('product/' . $editData->avatar);
            }
            Storage::disk('public')->put('product/' . $imageName, File::get($image));
        } else {
            $imageName = $editData->avatar;
        }

        $data = [
            'user_id' => auth()->user()->id,
            'category_id' => $request->category_id,
            'supplier_id' => $request->supplier_id,
            'name' => $request->name,
            'brand' => $request->brand,
            'code' => $request->code,
            'avatar' => $imageName,
            'purchase_price' => $request->purchase_price,
            'sale_price' => $request->sale_price,
            'manufacture' => $request->manufacture,
            'tax' => $request->tax,
        ];
 
        $editData->update($data);
        if ($request->qty || $request->unit || $request->conversion_rate || $request->base_unit) {
            $editData->stock->update([
                'qty' => $request->qty,
                'base_unit'=>$request->base_unit,
                'conversion_rate'=>$request->conversion_rate, 
                'unit' => $request->unit
        ]);
        }
        return response()->json(['message' => 'Product Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteData = Product::query()->findOrFail($id)->delete();
        return response()->json(['message' => 'Product deleted successfully!']);
    }
}
