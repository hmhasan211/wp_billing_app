<?php

namespace App\Http\Controllers\Api;

use App\Models\api\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\CategoryResource;

class CategoryContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filterIndex($search)
    {
        $allData = Category::query()->User();

        if($search == 'trashed'){
            $allData =      $allData ->withTrashed()
            ->whereNotNull('deleted_at')
            ->get();
        }elseif($search == 'inactive'){
            $allData =   $allData->where('is_active',0)
            ->get();
        }else{
            $allData =  $allData->where('is_active',1)
            ->get();
        }

        // return response()->json(['data' => $allData]);
        return CategoryResource::collection($allData);
    }



    public function search($field,$query)
    {  
        $allData = Category::query()->User();

        if($field == 'trashed'){
            $allData =      $allData->where('name', 'LIKE', "%$query%")
            ->withTrashed()
            ->whereNotNull('deleted_at')
            ->get();
           
        }elseif($field == 'inactive'){
            $allData =   $allData->where('is_active',0)
            ->where('name', 'LIKE', "%$query%")
            ->get();
        }else{
            $allData =  $allData->where('is_active',1)
            ->where('name', 'LIKE', "%$query%")
            ->get();
        }
        // dd($allData);
        // return new CategoryResource($allData);
        return response()->json(['data' => $allData]);
        //  dd($allData);
        
        // if ($allData->count() < 0) {
        //      return response()->json(['message' => "Sorry!! Your query doesn't match any data "]);
        // } else {
        //       return response()->json(['data' => $allData]);
        //     // return new CategoryResource($allData);
        // }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:20', Rule::unique('categories')->where('user_id', auth()->user()->id)->where('name', $request->get('name'))]
        ]);

        $addData = Category::query()->create([
            'user_id' => Auth::user()->id,
            'name' => $request->name
        ]);
        // return new CategoryResource($addData);
        return response()->json(['message' => 'Category created successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showData = Category::query()->User()->findOrFail($id);

        if (is_null($showData)) {
            return response()->json(['message' => 'Data not found!!']);
        } else {
            return new CategoryResource($showData);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:20'
        ]);

        $editCat = Category::query()->findOrFail($id)->update([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'is_active' => $request->is_active,
        ]);
        return response()->json(['message' => 'Category updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $catDelete = Category::query()->findOrFail($id)->delete();
        return response()->json(['message' => 'Category Deleted successfully!']);
    }
    //restore category
    public function restore($id)
    {
        $restore = Category::query()->User()->withTrashed()->findOrFail($id)->restore();
        // return $restore;
        return response()->json(['message' => 'This category has been restored !']);
    }
}
