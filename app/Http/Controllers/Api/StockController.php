<?php

namespace App\Http\Controllers\Api;

use App\Models\api\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StockCollection;
use App\Http\Resources\StockResource;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = Stock::query()->whereHas('product', function ($query) {
            return $query->where('user_id', auth()->user()->id);
        })->OrderBy('id', 'DESC')->get();
        // dd($allData);
        if ($allData->count() > 0) {
            return new StockCollection($allData);
        } else {
            return response()->json(['message' => 'No data found!']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required',
            'qty' => 'required',
            'unit' => 'required',
        ]);

        $addData = Stock::query()->create([
            'product_id' => $request->product_id,
            'base_unit' => $request->base_unit,
            'conversion_rate' => $request->conversion_rate,
            'qty' => $request->qty,
            'unit' => $request->unit
        ]);
        return response()->json(['message' => 'Stock created successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showData = Stock::query()->whereHas('product', function ($query) {
            return $query->where('user_id', auth()->user()->id);
        })->findOrFail($id);
        if (is_null($showData)) {
            return response()->json(['message' => 'Data not found!!']);
        } else {
            return new StockResource($showData);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editData = Stock::find($id);
        $this->validate($request, [
            'product_id' => 'required',
            'qty' => 'required',
            'unit' => 'required',
        ]);

        $data = [
            'product_id' => $request->product_id,
            'base_unit' => $request->base_unit,
            'conversion_rate' => $request->conversion_rate,
            'qty' => $request->qty,
            'unit' => $request->unit
        ];
        $editData->update($data);

        return response()->json(['message' => 'Product Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
