<?php

namespace App\Http\Controllers;

use App\Http\Resources\web\CategoryCollection;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //changeS Tatus
    public function status($id)
    {
        $status = Category::find($id);
        // dd($status);
        if ($status->is_active == 1) {
            $status->is_active = 0;
        } else {
            $status->is_active = 1;
        }
        $status->save();
        return response()->json([
            'msg' => 'Status has been changed!!',
        ], 200);
    }
    //blockStatus change
    public function blockStatus($id)
    {
        $status = Category::find($id);
        // dd($status);
        if ($status->is_blocked == 1) {
            $status->is_blocked = 0;
        } else {
            $status->is_blocked = 1;
        }
        $status->save();
        return response()->json([
            'msg' => 'Status has been changed!!',
        ], 200);
    }

    //ajax request
    public function getCategoriesJson()
    {
        return new CategoryCollection(Category::query()->with('user')->orderBy('id', 'DESC')->paginate(5));
        // return response()->json([
        //     'success' => 'OK',
        //     'data' => $categories
        // ], 200);
    }

    //search data
    public function search($query)
    {
        return new CategoryCollection(Category::query()->where('name', 'LIKE', "%$query%")->latest()->paginate(5));
    }
}
