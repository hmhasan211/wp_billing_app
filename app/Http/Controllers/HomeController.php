<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserCollection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function viewUser()
    {
        return view('user.index');
    }

    public function getUsers()
    {
        return new UserCollection(User::latest()->paginate(5));
        // dd($allUser);
        // return response()->json([
        //     'msg' => 'success',
        //     'data' => $allUser,
        // ], 200);
    }

    //search data
    public function search($query)
    {
        $users =  User::query()->where('name', 'LIKE', "%$query%")->orWhere('email', 'LIKE', "%$query%")->orWhere('mobile', 'LIKE', "%$query%")->latest()->paginate(5);
        return new UserCollection($users);
    }

    public function blockStatus($id)
    {
        $status = User::find($id);
        // dd($status);
        if ($status->is_blocked == 1) {
            $status->is_blocked = 0;
        } else {
            $status->is_blocked = 1;
        }
        $status->save();
        return response()->json([
            'msg' => 'Status has been changed!!',
        ], 200);
    }
}
