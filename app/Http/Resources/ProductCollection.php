<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($allData) {
                return [
                    'id' => $allData->id,
                    'user' => $allData->user->name ?? null,
                    'category' => [
                        'id'=>$allData->category->id ?? null,
                        'name'=>$allData->category->name ?? null,
                    ],
                    'supplier' => [
                        'id'=>$allData->supplier->id ?? null,
                        'name'=>$allData->supplier->name ?? null,
                    ],
                    'name' => $allData->name,
                    'brand' => $allData->brand,
                    'code' => $allData->code,
                    'avatar' => asset('/storage/product/' . $allData->avatar),
                    'purchase_price' => $allData->purchase_price,
                    'sale_price' => $allData->sale_price,
                    'manufacture' => $allData->manufacture,
                    'tax' => $allData->tax,
                    'is_active' => $allData->is_active,
                    'stock' => [
                        'id' => $allData->stock->id ?? null,
                        'product_id' => $allData->stock->product_id ?? null,
                        'base_unit' => $allData->stock->base_unit ?? null,
                        'conversion_rate' => $allData->stock->conversion_rate ?? null,
                        'qty' => $allData->stock->qty ?? null,
                        'unit' => $allData->stock->unit ?? null,
                    ],
                ];
            }),
        ];
    }
}