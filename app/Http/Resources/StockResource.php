<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StockResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'user' => $this->product->user->name,
            // 'product_id' => $this->product->name,
            'product_id' => $this->product_id,
            'base_unit' => $this->base_unit,
            'conversion_rate' => $this->conversion_rate,
            'qty' => $this->qty,
            'unit' => $this->unit
        ];
    }
}
