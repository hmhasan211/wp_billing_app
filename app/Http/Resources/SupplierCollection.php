<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SupplierCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($allData) {
                return [
                    'id' => $allData->id,
                    'user_id' => $allData->User->name,
                    'name' => $allData->name,
                    'email' => $allData->email,
                    'phone' => $allData->phone,
                    'address' => $allData->address,
                    'avatar' => asset('/storage/supplier/' . $allData->avatar),
                    'dob' => $allData->dob,
                    'balance' => $allData->balance,
                ];
            }),
        ];
    }
}
