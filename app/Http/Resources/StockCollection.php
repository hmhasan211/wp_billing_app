<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StockCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($allData) {
                return [
                    'id' =>$allData->id,
                    'user' => $allData->product->User->name,
                    // 'product_id' => $allData->product->name,
                    'product_id' => $allData->product_id,
                    'base_unit' => $allData->base_unit,
                    'conversion_rate' => $allData->conversion_rate,
                    'qty' => $allData->qty,
                    'unit' => $allData->unit
                ];
            }),
        ];
    }
}
