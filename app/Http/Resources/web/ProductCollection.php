<?php

namespace App\Http\Resources\web;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    
    public function toArray($request)
    {
        return [
                'data' => $this->collection->transform(function ($allData) {
                return [
                    'id' => $allData->id,
                    'user_id' => $allData->user->name,
                    'category_id' => $allData->category->name ?? null,
                    'supplier_id' => $allData->supplier->name ?? null,
                    'name' => $allData->name,
                    'brand' => $allData->brand,
                    'avatar' => asset('/storage/product/' . $allData->avatar),
                    'is_blocked' => $allData->is_blocked,
                    'stock'=>[
                        'qty'=>$allData->stock->qty ?? null,
                        'unit'=>$allData->stock->unit ?? null,
                    ]
                ];
            })
        ];
    }
}

