<?php

namespace App\Http\Resources\web;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CustomerCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($customer) {
                return [
                    'id' => $customer->id,
                    'user_id' => $customer->user->name,
                    'name' => $customer->name,
                    'phone' => $customer->phone,
                    'address' => $customer->address,
                    'email' => $customer->email,
                    'avatar' => asset('/storage/customer/' . $customer->avatar),
                    'is_blocked' => $customer->is_blocked,
                ];
            })
        ];
    }
}
