<?php

namespace App\Http\Resources\web;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($category) {
                return [
                    'id' => $category->id,
                    'user_id' => $category->user->name,
                    'name' => $category->name,
                    'is_active' => $category->is_active,
                    'is_blocked' => $category->is_blocked
                ];
            })
        ];
    }
}
