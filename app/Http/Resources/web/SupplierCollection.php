<?php

namespace App\Http\Resources\web;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SupplierCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($supplier) {
                return [
                    'id' => $supplier->id,
                    'user_id' => $supplier->user->name,
                    'name' => $supplier->name,
                    'phone' => $supplier->phone,
                    'address' => $supplier->address,
                    'email' => $supplier->email,
                    'avatar' => asset('/storage/supplier/' . $supplier->avatar),
                    'is_blocked' => $supplier->is_blocked,
                ];
            })
        ];
    }
}
