<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResouce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => $this->user->name,
            // 'category' => $this->category->name ?? null,
            // 'supplier' => $this->supplier->name ?? null,
            'category' => [
                'id'=>$this->category->id ?? null,
                'name'=>$this->category->name ?? null,
            ],
            'supplier' => [
                'id'=>$this->supplier->id ?? null,
                'name'=>$this->supplier->name ?? null,
            ],
            'name' => $this->name,
            'brand' => $this->brand,
            'code' => $this->code,
            'avatar' => asset('/storage/product/' . $this->avatar),
            'purchase_price' => $this->purchase_price,
            'sale_price' => $this->sale_price,
            'manufacture' => $this->manufacture,
            'tax' => $this->tax,
            'is_active' => $this->is_active,
            'message' => $this->message,
            'stock' => [
                        'id' => $this->stock->id ?? null,
                        'product_id' => $this->stock->product_id ?? null,
                        'base_unit' => $this->stock->base_unit ?? null,
                        'conversion_rate' => $this->stock->conversion_rate ?? null,
                        'qty' => $this->stock->qty ?? null,
                        'unit' => $this->stock->unit ?? null,
                    ],
        ];
    }
}