<?php

namespace App\Models\api;

use App\Models\api\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Stock extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'product_id', 'base_unit', 'conversion_rate', 'qty', 'unit'];


    /**
     * The product that belong to the Stock
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
