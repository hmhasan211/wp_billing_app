<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id', 'name','is_active'];

    public function scopeUser($query)
    {
        $query->where('user_id', Auth::user()->id)->where('is_blocked', 0);
    }
}
