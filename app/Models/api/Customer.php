<?php

namespace App\Models\api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Customer extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id',
        'name',
        'phone',
        'email',
        'address',
        'dob',
        'avatar',
        'balance',
        'is_active',
    ];

    public function scopeUser($query)
    {
        $query->where('user_id', auth()->user()->id)->where('is_active', 1)->where('is_blocked', 0);
    }
}
