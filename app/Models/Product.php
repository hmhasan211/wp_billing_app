<?php

namespace App\Models;

use App\Models\Stock;
use App\Models\Category;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;


    //get user Information
    public function user()
    {
        return $this->belongsTo(User::class)->select('id', 'name');
    }

    //get category Information
    public function category()
    {
        return $this->belongsTo(Category::class)->select('id', 'name');
    }
    
    //get Supplier Information
    public function supplier()
    {
        return $this->belongsTo(Supplier::class)->select('id', 'name');
    }

    public function stock()
    {
        return $this->hasOne(Stock::class)->select('qty', 'unit');
    }
}
