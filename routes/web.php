<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//optimize command
Route::get('reboot', function () {
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    echo '<div><h1 style="text-align: center">System Rebooted</h1></div>';
});
Route::get('migrate-fresh', function () {
    Artisan::call('migrate:fresh');
    echo '<div><h1 style="text-align: center">Migrate fresh Successfully</h1></div>';
});
Route::get('migrate', function () {
    Artisan::call('migrate');
    echo '<div><h1 style="text-align: center">Migrate Successfully</h1></div>';
});
Route::get('storage-link', function () {
    Artisan::call('storage:link');
    echo '<div><h1 style="text-align: center">storage link created Successfully</h1></div>';
});

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/',[LoginController::class]);

Auth::routes();


Route::get('/{any}', [HomeController::class, 'index'])->where('name', '.*');

Route::group(['middleware' => ['auth:sanctum']], function () {

    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('/user/view', [HomeController::class, 'viewUser'])->name('users.view');
    Route::get('/get/users', [HomeController::class, 'getUsers']);
    Route::get('/users/search/{query}', [HomeController::class, 'search']);
    Route::PUT('/user/block/{id}', [HomeController::class, 'blockStatus']);
    //category
    Route::get('/category/all', [CategoryController::class, 'getCategoriesJson']);
    Route::PUT('/category/status/{id}', [CategoryController::class, 'status']);
    Route::PUT('/category/block/{id}', [CategoryController::class, 'blockStatus']);
    Route::get('/category/search/{query}', [CategoryController::class, 'search']);
    //customer
    Route::get('/customer/all', [CustomerController::class, 'getCustomerJson']);
    Route::PUT('/customer/status/{id}', [CustomerController::class, 'status']);
    Route::PUT('/customer/block/{id}', [CustomerController::class, 'blockStatus']);
    Route::get('/customer/search/{query}', [CustomerController::class, 'search']);
     //supplier
     Route::get('/supplier/all', [SupplierController::class, 'getSupplierJson']);
     Route::PUT('/supplier/status/{id}', [SupplierController::class, 'status']);
     Route::PUT('/supplier/block/{id}', [SupplierController::class, 'blockStatus']);
     Route::get('/supplier/search/{query}', [SupplierController::class, 'search']);
    //products
     Route::get('/product/all', [ProductController::class, 'getProductJson']);
     Route::PUT('/product/block/{id}', [ProductController::class, 'blockStatus']);
     Route::get('/product/search/{query}', [ProductController::class, 'search']);
});
