<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthContoller;
use App\Http\Controllers\Api\StockController;
use App\Http\Controllers\Api\CategoryContoller;
use App\Http\Controllers\Api\InvoiceController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\SupplierController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//public Routes
Route::post('/register', [AuthContoller::class, 'register']);
Route::post('/login', [AuthContoller::class, 'login']);

//protected routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    //User profile
    Route::get('/user/profile', [AuthContoller::class, 'showProfile']);
    //User update
    Route::post('/user/update', [AuthContoller::class, 'update']);
    //User update password
    Route::post('/user/update/password', [AuthContoller::class, 'updatePassword']);
    //User Logout
    Route::post('/logout', [AuthContoller::class, 'logout']);

    //Category
    Route::apiResource('/category', CategoryContoller::class);
    Route::get('/categoryfilter/{search}', [CategoryContoller::class, 'filterIndex']);
    Route::get('search/category/{field}/{query}', [CategoryContoller::class, 'search']);
    Route::post('/category/restore/{id}', [CategoryContoller::class, 'restore']);

    //Customer
    Route::apiResource('/customer', CustomerController::class);
    Route::get('search/customer/{query}', [CustomerController::class, 'search']);
    
    //Supplier
    Route::apiResource('/supplier', SupplierController::class);
    Route::get('search/supplier/{query}', [SupplierController::class, 'search']);

    //Product
    Route::apiResource('/product', ProductController::class);
    Route::get('search/product/{query}', [ProductController::class, 'search']);
    Route::post('/increment/product/stock/{id}', [ProductController::class, 'stockIncrement']);

    //Stock
    Route::apiResource('/stock', StockController::class);

    //invoice
    Route::get('/invoice',[InvoiceController::class, 'index']);
    Route::post('/invoice',[InvoiceController::class, 'store']);
});
