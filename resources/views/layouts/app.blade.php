@include('includes.header')
@yield('style')

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper" id="app">
        <!-- Navbar -->
        @include('includes.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('includes.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" >
            {{-- @yield('content') --}}
            <router-view></router-view>
        </div>
        <!-- /.content-wrapper -->

        @include('includes.footer')
        @yield('script')
</body>

</html>
