     <footer class="main-footer">
         <div class="float-right d-none d-sm-block">
             <b>Version</b> 3.0.5
         </div>
         <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
         reserved.
     </footer>

     <!-- Control Sidebar -->
     {{-- <aside class="control-sidebar control-sidebar-dark">
         <!-- Control sidebar content goes here -->
     </aside> --}}
     <!-- /.control-sidebar -->
     </div>
     <!-- ./wrapper -->
     <!-- Scripts -->
     <script src="{{ asset('js/app.js') }}" defer></script>
     <!-- jQuery -->
     <script src="{{ asset('/') }}plugins/jquery/jquery.min.js"></script>
     <!-- Bootstrap 4 -->
     <script src="{{ asset('/') }}plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
     <!-- AdminLTE App -->
     <script src="{{ asset('/') }}dist/js/adminlte.min.js"></script>
     <!-- AdminLTE for demo purposes -->
     <script src="{{ asset('/') }}dist/js/demo.js"></script>
     <!-- ChartJS 1.0.2 -->
     {{-- <script src="{{ asset('plugins/chartjs-old/Chart.min.js') }}"></script> --}}
     {{-- <script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script> --}}
