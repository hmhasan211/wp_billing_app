import * as actions from '../../action-types'
import * as mutations from '../../mutation-types'
import Axios from 'axios'

export default {
    [actions.GET_CATEGORIES]({ commit }){
        Axios.get('/cats/all')
            .then((res) => {
                // console.log(res.data)
                if (res.data.success == 'OK') {
                    //  console.log(res.data)
                    commit(mutations.SET_CATEGORIES, res.data.data)
                }
            }).catch(err => {
                console.log(err.response)
        })
    }
}