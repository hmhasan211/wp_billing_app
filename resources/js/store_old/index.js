import { createStore } from 'vuex'


//modules import
import categories from './modules/categories'

export default createStore({

  modules: {
    categories
    }
})
