import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from '../components/dashboard.vue'
import CategoryIndex from '../components/category/index.vue'
import CategoryCreate from '../components/category/create.vue'
import UserIndex from '../components/user/index.vue'
import CustomerIndex from '../components/customer/index.vue'
import SupplierIndex from '../components/supplier/index.vue'
import ProductIndex from '../components/product/index.vue'

const routes = [
    {
    path: '/home',
    name: 'Dashboard',
    component: Dashboard
    },
    //category route
    {
    path: '/category',
    name: 'CategoryIndex',
    component: CategoryIndex
    },
    {
    path: '/category/create',
    name: 'CategoryCreate',
    component: CategoryCreate
    },
     //customer route
    {
    path: '/customer',
    name: 'CustomerIndex',
    component: CustomerIndex
    },
     //customer route
    {
    path: '/supplier',
    name: 'SupplierIndex',
    component: SupplierIndex
    },

    //user routes
    {
    path: '/user',
    name: 'UserIndex',
    component: UserIndex
    },
   //products
    {
    path: '/product',
    name: 'ProductIndex',
    component: ProductIndex
    },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
