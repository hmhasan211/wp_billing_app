import axios from "axios";
axios.defaults.baseURL = "http://127.0.0.1:8000";

export const product = {
    state: {
        products: [],
        pagination: {
            current_page: 1,
        },
    },

    getters: {
        getProducts: (state) => state.products,
        paginations: (state) => state.pagination,
    },

    actions: {
        async fetchProducts({ commit, state }) {
            await axios
                .get("/product/all?page=" + state.pagination.current_page)
                .then((response) => {
                    console.log(response);
                    commit("SET_PRODUCT", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                })
                .catch((e) => {
                    console.log(e);
                });
        },

        async searchProduct({ commit ,state}, data) {
            await axios
                .get(
                    "/product/search/" +
                        data +
                        "?page=" +
                        state.pagination.current_page
                )
                .then((response) => {
                    console.log(response.data);
                    commit("SET_PRODUCT", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                    // this.$Progress.finish();
                })
                .catch((e) => {
                    console.log(e);
                    // this.$Progress.fail();
                });
        },
        async productBlockStatus({commit},index) {
            await axios
              .put("/product/block/" + index)
              .then((res) => {
                   console.log(res);
              })
              .catch((err) => {
                console.log(err);
              });
          },
    },

    mutations: {
        SET_PRODUCT: (state, data) => (state.products = data),
        SET_PAGINATE: (state, meta) => (state.pagination = meta),
    },
};
