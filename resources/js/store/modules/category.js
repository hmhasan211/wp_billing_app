import axios from "axios";
axios.defaults.baseURL = "http://127.0.0.1:8000";

export const category = {
    state: {
        query: "",
        categories: [],
        pagination: {
            current_page: 1,
        },
    },

    getters: {
        categories: (state) => state.categories,
        paginations: (state) => state.pagination,
    },

    actions: {
        async fetchCategory({ commit, state }) {
            await axios
                .get("/category/all?page=" + state.pagination.current_page)
                .then((response) => {
                    console.log(response);
                    commit("SET_CATEGORY", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                })
                .catch((e) => {
                    console.log(e);
                });
        },

        async searchCategory({ commit ,state}, data) {
            await axios
                .get(
                    "/category/search/" +
                        data +
                        "?page=" +
                        state.pagination.current_page
                )
                .then((response) => {
                    console.log(response.data);
                    commit("SET_CATEGORY", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                    // this.$Progress.finish();
                })
                .catch((e) => {
                    console.log(e);
                    // this.$Progress.fail();
                });
        },
        async catsBlockStatus({commit},index) {
            await axios
              .put("/category/block/" + index)
              .then((res) => {
                   console.log(res);
               
              })
              .catch((err) => {
                console.log(err);
              });
          },
    },

    mutations: {
        SET_CATEGORY: (state, data) => (state.categories = data),
        SET_PAGINATE: (state, meta) => (state.pagination = meta),
    },
};
