import axios from "axios";
axios.defaults.baseURL = "http://127.0.0.1:8000";

export const supplier = {
    state: {
        query: "",
        suppliers: [],
        pagination: {
            current_page: 1,
        },
    },

    getters: {
        suppliers: (state) => state.suppliers,
        paginations: (state) => state.pagination,
    },

    actions: {
        async fetchSupplier({ commit, state }) {
            await axios
                .get("/supplier/all?page=" + state.pagination.current_page)
                .then((response) => {
                    console.log(response);
                    commit("SET_SUPPLIER", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                })
                .catch((e) => {
                    console.log(e);
                });
        },

        async searchSupplier({ commit ,state}, data) {
            await axios
                .get(
                    "/supplier/search/" +
                        data +
                        "?page=" +
                        state.pagination.current_page
                )
                .then((response) => {
                    console.log(response.data);
                    commit("SET_SUPPLIER", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                    // this.$Progress.finish();
                })
                .catch((e) => {
                    console.log(e);
                    // this.$Progress.fail();
                });
        },
        async suppBlockStatus({commit},index) {
            await axios
              .put("/supplier/block/" + index)
              .then((res) => {
                   console.log(res);
               
              })
              .catch((err) => {
                console.log(err);
              });
          },
    },

    mutations: {
        SET_SUPPLIER: (state, data) => (state.suppliers = data),
        SET_PAGINATE: (state, meta) => (state.pagination = meta),
    },
};
