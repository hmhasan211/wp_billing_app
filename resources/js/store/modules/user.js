import axios from "axios"
axios.defaults.baseURL = 'http://127.0.0.1:8000';


export const user = {


    state: {
        users: [],
        pagination: {
            current_page: 1,
        },
    },

    getters: {
        users: (state) => state.users,
        paginations: (state) => state.pagination,
    },

    actions: {
        async fetchUsers({ commit, state }) {
            await axios
                .get("/get/users?page=" + state.pagination.current_page)
                .then((response) => {
                    console.log(response);
                    commit("SET_USER", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                })
                .catch((e) => {
                    console.log(e);
                });
        },

        async searchUser({ commit ,state}, data) {
            await axios
                .get(
                    "/users/search/" +
                        data +
                        "?page=" +
                        state.pagination.current_page
                )
                .then((response) => {
                    console.log(response.data);
                    commit("SET_USER", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                    // this.$Progress.finish();
                })
                .catch((e) => {
                    console.log(e);
                    // this.$Progress.fail();
                });
        },
        async userBlockStatus({commit},index) {
            await axios
              .put("/user/block/" + index)
              .then((res) => {
                   console.log(res);
              })
              .catch((err) => {
                console.log(err);
              });
          },
    },

    mutations: {
        SET_USER: (state, data) => (state.users = data),
        SET_PAGINATE: (state, meta) => (state.pagination = meta),
    },

}