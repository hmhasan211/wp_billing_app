import axios from "axios";
axios.defaults.baseURL = "http://127.0.0.1:8000";

export const customer = {
    state: {
        query: "",
        customers: [],
        pagination: {
            current_page: 1,
        },
    },

    getters: {
        allcustomers: (state) => state.customers,
        paginations: (state) => state.pagination,
    },

    actions: {
        async fetchCustomer({ commit, state }) {
            await axios
                .get("/customer/all?page=" + state.pagination.current_page)
                .then((response) => {
                    console.log(response);
                    commit("SET_CUSTOMER", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                })
                .catch((e) => {
                    console.log(e);
                });
        },

        async searchCustomer({ commit ,state}, data) {
            await axios
                .get(
                    "/customer/search/" +
                        data +
                        "?page=" +
                        state.pagination.current_page
                )
                .then((response) => {
                    console.log(response.data);
                    commit("SET_CUSTOMER", response.data.data);
                    commit("SET_PAGINATE", response.data.meta);
                    // this.$Progress.finish();
                })
                .catch((e) => {
                    console.log(e);
                    // this.$Progress.fail();
                });
        },
        async customerBlockStatus({commit},index) {
            await axios
              .put("/customer/block/" + index)
              .then((res) => {
                   console.log(res);
               
              })
              .catch((err) => {
                console.log(err);
              });
          },
    },

    mutations: {
        SET_CUSTOMER: (state, data) => (state.customers = data),
        SET_PAGINATE: (state, meta) => (state.pagination = meta),
    },
};
