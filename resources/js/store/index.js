import { createStore } from "vuex";
import { category } from "./modules/category";
import { user } from "./modules/user";
import { customer } from "./modules/customer";
import { supplier } from "./modules/supplier";
import { product } from "./modules/product";
export default createStore({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        category,
        user,
        customer,
        supplier,
        product
    },
});
