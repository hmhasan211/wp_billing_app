require("./bootstrap");

import { createApp } from "vue";
import router from "./router";
import store from "./store";
import Toaster from "@meforma/vue-toaster";

import Dashboard from "./components/dashboard.vue";
import Pagination from "./components/pagination.vue";
//category
import category from "./components/category/index.vue";
import CategoryCreate from "./components/category/create.vue";
//user
import userIndex from "./components/user/index.vue";
//customer
import CustomerIndex from "./components/customer/index.vue";
//Supplier
import SupplierIndex from "./components/supplier/index.vue";
//product
import ProductIndex from "./components/product/index.vue";

const app = createApp({});

app
  .component("Dashboard", Dashboard)
  .component("catIndex", category)
  .component("CustomerIndex", CustomerIndex)
  .component("SupplierIndex", SupplierIndex)
  .component("ProductIndex", ProductIndex)
  .component("catCreate", CategoryCreate)
  .component("userIndex", userIndex)
  .component("pagination", Pagination);

app.use(Toaster, {
  position: "top-right",
});

app.use(router).use(store).use(Toaster).mount("#app");
